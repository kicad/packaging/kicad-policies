== KiCad Policy Templates

These policy templates are meant for IT deployments using Group Policies on Windows.

=== Summary
[cols="1,1"]
|===
|Policy Name
|Description

|<<#sslrevoke,curl -> SSL Certificate Revocation Checks>>
|Controls the SSL Certificate Revocation Check behavior

|<<#datacollect,Data Collection>>
|Enable or disable the anonymous reporting of crashes and special event data

|<<#thirdparty-pcm,Third Party Content -> Plugin and Content Manager>>
|Enable or disable the Plugin and Content Manager

|=== 

=== Details
[#sslrevoke]
==== curl | SSL Certificatie Revocation Checks

Configures the behavior of curl when schannels attempts to validate certificates.
curl using schannel by default will attempt to validate the certificate not only for CA trust but also revocation status.

In corporate Windows environments, misconfigured certificates with invalid certificate revocation list (CRLs) URLs or OCSP Uris can be present from MITM firewalls. This policy allows modifying the revocation check behavior from the default if the certificates cannot be made proper.

*Compatibility:* KiCad 7.0.7 and above

===== Windows (GPO)

```
Software\Policies\KiCad\KiCad\curl\SslRevoke = 0x2 | 0x1 | 0x0
```


*Values*

[cols="1,1"]
|===
| Value
| Description

| 0x00
| Default - always perform revocation checks

| 0x01
| Best Effort - attempt to perform checks but silently continue if they fail to reach CRL/OCSP servers

| 0x02
| None - do not perform any revocation checks

|===

Not configuring this policy results in default behavior

[#datacollect]
==== Data Collection

KiCad can anonymously report crashes and special event data to dvelopers in order to aid identifying critical bugs across the user base and help profile functionality to guide improvements.

All crash reports omit any personally idenfitable information (PII) and never upload any design data. A completely random unique GUID is generated per user install for the explicit purpose of only crash reporting.


*Compatibility:* KiCad 6.99 and above

===== Windows (GPO)

```
Software\Policies\KiCad\KiCad\DataCollection = 0x1 | 0x0
```

Not configuring this policy leaves the choice up to the end user.


[cols="1,1"]
|===
| Value
| Description

| 0x00
| Turned off, end user cannot enable

| 0x01
| Turned on, end user cannot enable
|===


[#thirdparty-pcm]
==== Third Party Content -> Plugin and Content Manager

KiCad contains a Plugin and Content Manager (PCM) that allows users to download plugins and CAD libraries to enhance their KiCad experience.


*Compatibility:* KiCad 6.99 and above

===== Windows (GPO)

```
Software\Policies\KiCad\KiCad\ThirdPartyContent = 0x1 | 0x0
```

Not configuring this policy leaves the choice up to the end user.